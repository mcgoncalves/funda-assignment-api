This repository contains an assignment made for funda.nl. 

I decided to implement it in .Net core using Swashbuckle nuget which integrates Swagger UI.

There are two endpoints as requested:
 - one to get the top 10 makelaars of Amsterdam listed for sale;
 - other to get the top 10 makelaars of Amsterdam with tuin (garden) listed for sale;
 
I should have included tests, but unfortunately, I haven't had the available time to dedicate to this assignment, but I tried my best.