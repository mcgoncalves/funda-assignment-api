﻿using FundaAssignment.Models;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;

namespace FundaAssignment.Controllers
{
    [Route("api/")]
    [ApiController]
    public class FundaController : Controller
    {
        private async Task<IActionResult> GetFundaOrderedProperties(string filter = "", int totalResults = 10)
        {
            using (var client = new HttpClient())
            {
                try
                {
                    client.BaseAddress = new Uri("http://partnerapi.funda.nl");
                    var tempKey = "ac1b0b1572524640a0ecc54de453ea9f";
                    var currentPage = 1;
                    const int pageSize = 200;
                    var propertiesList = new List<FundaListedObject>();
                    do
                    {
                        //go through every results page and store all results into propertiesList
                        await client.GetAsync($"/feeds/Aanbod.svc/json/{tempKey}/?type=koop&zo=/amsterdam{filter}/&page={currentPage}&pagesize={pageSize}")
                        .ContinueWith(async (task) =>
                        {
                            if (task.IsCompletedSuccessfully)
                            {
                                var stringResult = await task.Result.Content.ReadAsStringAsync();
                                var convertedResponse = JsonConvert.DeserializeObject<FundaAPIResponse>(stringResult);
                                if (convertedResponse.Objects.Any())
                                {
                                    propertiesList.AddRange(convertedResponse.Objects);
                                }
                                //this needs to stop when current page is equal to total pages
                                currentPage = (convertedResponse.Paging.AantalPaginas == currentPage) ? 0 : currentPage + 1;
                            }
                        });
                    }
                    while (currentPage != 0);

                    //group results by makelaar, then order by property count for each makelaar, only take the ones that we want
                    var top10makelaars = propertiesList.GroupBy(g => g.MakelaarId)
                        .OrderByDescending(x => x.Count())
                        .Take(totalResults)
                        .ToDictionary(kvp => kvp.First().MakelaarNaam, kvp => kvp.Count());

                    return Ok(top10makelaars.Select((kvp, index) => $"{index+1} -> {kvp.Key} - ({kvp.Value} properties)"));
                }
                catch (HttpRequestException httpRequestException)
                {
                    return BadRequest($"Error accessing funda API: {httpRequestException.Message}");
                }
            }
        }


        /// <summary>
        /// The top 10 makelaar's in Amsterdam, which have the most object listed for sale.
        /// </summary>
        /// {city}
        [HttpGet("top-makelaars/amsterdam")]
        public async Task<IActionResult> TopMakelaars()
        {
            return await GetFundaOrderedProperties();
        }

        /// <summary>
        /// The top 10 makelaar's in Amsterdam, but only with tuin, which are listed for sale.
        /// </summary>
        /// {city}/{prop}
        [HttpGet("top-makelaars/amsterdam/tuin")]
        public async Task<IActionResult> TopMakelaarsWithTuin()
        {
            return await GetFundaOrderedProperties(filter: "/tuin");
        }
    }
}
