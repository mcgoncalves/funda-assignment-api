﻿namespace FundaAssignment.Models
{
    public class FundaAPIResponse
    {
        public FundaListedObject[] Objects { get; set; }
        public int TotaalAantalObjecten { get; set; }
        public FundaPaging Paging { get; set; }
        //Previous
        public string VorigeUrl { get; set; }
        //Next
        public string VolgendeUrl { get; set; }
    }

    public class FundaListedObject
    {
        public int MakelaarId { get; set; }
        public string MakelaarNaam { get; set; }
        public string Woonplaats { get; set; }
        //This is a status type, it must be available = StatusBeschikbaar
        public string VerkoopStatus { get; set; }
    }

    public class FundaPaging
    {
        //Total pages
        public int AantalPaginas { get; set; }
        //Current page
        public int HuidigePagina { get; set; }
    }
}
